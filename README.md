# Módulo de cadastro e login

Neste repositório resite o códigos relacionados ao módulo de cadastro
e login do sistema Proway Bank.

## Protótipos das telas do sistema

[figma](https://www.figma.com/file/vi8Sw8iDT1eZEIjsW8Ayt3/Primeira-Turma-Capgemini-team-library?node-id=0%3A1)

## Arquitetura do projeto

O sistema está todo baseado em separação dos componentes relativos a que 
funcionalidade do crédito ela está resolvendo, e separando os
arquivos html,css e javascript.

Além disso também possui a pasta components, o qual devido a limitação 
inicial do projeto em que deveria utilizar hmtl puro, não havia como 
componentizar as páginas, então foram criados um footer e um header
o qual devem ser importados em todas as outras funcionalidades através
da utilização de um `<iframe>`, segue um template base na raiz da pasta
components.

## Controle do fluxo de desenvolvimento

O controle das tarefas, backlogs e processo do projeto, são todos 
feitos via o Jira issues, que é acessível direto pelo repositório,
a escolha se deve a integração direta entre o sistema o que facilita
a decisão, além de trazer mais possibilidades de controle, diferente
 do trello que possui algumas limitações.

## Metodologia de desenvolvimento com git

A metodologia utilizada é o [gitflow](https://www.atlassian.com/br/git/tutorials/comparing-workflows/gitflow-workflow).
Em resumo, a develop sempre deve ser a mais atualizada e branchs de
features devem ser criadas a partir da develop, sendo também empregado
a necessidade de pull requests para review de código e garantir uma
padronização mínima.

### Ferramentas para o git

- [gitkraken](https://www.gitkraken.com/)
- [gitahead](https://gitahead.github.io/gitahead.com/)

Estas são ferramentas indicadas para visualização e simplificação da
utilização do git para o desenvolvimento, segue também esta configuração
do [gitkraken com gitflow](https://support.gitkraken.com/git-workflows-and-extensions/git-flow/)
para utilização do gitflow sem necessitar da utilização de linha de comando.
